 
 U S E   [ D B A R e s o u r c e s ]  
 G O  
 / * * * * * *   O b j e c t :     S t o r e d P r o c e d u r e   [ d b o ] . [ C a p t u r e G a t e w a y R e p l i c a t i o n F o r D R ]         S c r i p t   D a t e :   2 / 6 / 2 0 1 9   2 : 0 3 : 1 3   P M   * * * * * * /  
 S E T   A N S I _ N U L L S   O N  
 G O  
 S E T   Q U O T E D _ I D E N T I F I E R   O N  
 G O  
  
  
 - -   = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =  
 - -   A u t h o r : 	 	 S a v i n a   S a r a t h  
 - -   C r e a t e   d a t e :   0 8   F e b   2 0 1 9  
 - -   D e s c r i p t i o n : 	 R e - i n s t a t i n g   t h e   r e p l i c a t i o n   j o b s   f o r   a l l   t h e   C a p t u r e   G a t e w a y   d a t a b a s e s    
 - - 	 	 	 	 f r o m   t h e   A P P D B   t o   t h e   R e p o r t i n g   S e r v e r   i n   t h e   e v e n t   o f   a   D R   s c e n a r i o .  
 - -   R e v i s i o n   H i s t o r y :    
 - -   = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =  
  
 A L T E R   P R O C E D U R E   [ d b o ] . [ C a p t u r e G a t e w a y R e p l i c a t i o n F o r D R ]  
 A S  
  
  
 B E G I N   T R Y  
 B E G I N   T R A N  
  
 D E C L A R E   @ P u b l i c a t i o n N a m e   N V A R C H A R ( M A X )    
 D E C L A R E   @ C a p t u r e D B N a m e   N V A R C H A R ( M A X )      
 D E C L A R E   @ S Q L   N V A R C H A R ( M A X )  
 D E C L A R E   @ S t u d y E n v i r o n m e n t   V A R C H A R ( 2 0 )    
 D E C L A R E   @ E x c o I d e n t i f i e r   N V A R C H A R ( M A X )  
 D E C L A R E   @ R e p o r t S e r v e r   N V A R C H A R ( M A X )   =   ' C 1 D G T R V D B 0 2 '  
 D E C L A R E   @ R e p o r t i n g D B N a m e   N V A R C H A R ( M A X )  
  
 - - S e l e c t i n g   a l l   t h e   s t u d i e s   i n   L i v e   s t a t e s   i n t o   # L i v e _ U a t _ S t u d i e s  
 s e l e c t   g t . E x c o I d e n t i f i e r  
 , s i . A c t i v e  
 , ' L i v e '   A s   S t u d y E n v i r o n m e n t  
 , C O N C A T ( g t . E x c o I d e n t i f i e r , ' _ L i v e _ C a p t u r e ' )   A S   C a p t u r e D b N a m e  
   I N T O   # L i v e _ U a t _ S t u d i e s    
   f r o m   G a t h e r _ R 3 _ L i v e . . S t u d y I n s t a n c e s   s i    
 J O I N   G a t h e r _ R 3 . . G a t h e r S t u d i e s     g t   O N   s i . G a t h e r S t u d y I d   =   g t . G a t h e r S t u d y I d    
 W H E R E   s i . A c t i v e   =   1  
  
 - - S e l e c t i n g   a l l   t h e   s t u d i e s   i n   U A T   s t a t e s   i n t o   # L i v e _ U a t _ S t u d i e s  
 I N S E R T   I N T O    
 # L i v e _ U a t _ S t u d i e s  
 ( E x c o I d e n t i f i e r  
 , A c t i v e  
 , S t u d y E n v i r o n m e n t  
 , C a p t u r e D b N a m e )  
 s e l e c t   g t . E x c o I d e n t i f i e r  
 , s i . A c t i v e  
 ,   ' U a t '    
 , C O N C A T ( g t . E x c o I d e n t i f i e r , ' _ U a t _ C a p t u r e ' )    
 f r o m   G a t h e r _ R 3 _ U a t . . S t u d y I n s t a n c e s   s i    
 J O I N   G a t h e r _ R 3 . . G a t h e r S t u d i e s     g t   O N   s i . G a t h e r S t u d y I d   =   g t . G a t h e r S t u d y I d    
 W H E R E   s i . A c t i v e   =   1  
  
 - - D e c l a r i n g   c u r s o r   t o   l o o p   t h r o u g h   t h e   t e m p   t a b l e   t o   r e - i n s t a t e   r e p l i c a t i o n   f o r   a l l   t h e   a l l   t h e   C a p t u r e   G a t e w a y   D a t a b a s e s  
 D E C L A R E   C G D B c s r   C U R S O R   F A S T _ F O R W A R D   F O R  
 S E L E C T   E x c o I d e n t i f i e r   , S t u d y E n v i r o n m e n t ,   C a p t u r e D b N a m e   F R O M   # L i v e _ U a t _ S t u d i e s    
 O P E N   C G D B c s r  
 F E T C H   N E X T   F R O M   C G D B c s r   I N T O   @ E x c o I d e n t i f i e r , @ S t u d y E n v i r o n m e n t ,   @ C a p t u r e D B N a m e  
  
 W H I L E   @ @ F E T C H _ S T A T U S   =   0  
 B E G I N  
  
 - - C h e c k i n g   i f   t h e   C a p t u r e   D a t a b a s e   h a s   b e e n   r e - i n s t a t e d    
 I F   E X I S T S   ( S E L E C T   n a m e    
     F R O M   s y s . d a t a b a s e s   W H E R E   n a m e   =   @ C a p t u r e D B N a m e )  
 B E G I N    
 S E T   @ P u b l i c a t i o n N a m e   =   C O N C A T (  
 	 	 	 	 	 	 ' R e p o r t i n g S e r v e r '  
 	 	 	 	 	 	 , ' _ '  
 	 	 	 	 	 	 , @ C a p t u r e D B N a m e  
 	 	 	 	 	 	 )  
  
 S E T   @ R e p o r t i n g D B N a m e   =   C O N C A T   (  
 	 	 	 @ E x c o I d e n t i f i e r  
 	 	 	 , ' _ '  
 	 	 	 , @ S t u d y E n v i r o n m e n t  
 	 	 	 , ' _ R e p o r t i n g '  
 	 	 	 )  
 - - C h e c k i n g   i f   t h e   P u b l i c a t i o n   e x i s t s  
 - - S E T   @ S Q L   =   N ' E x e c   ' + Q U O T E N A M E ( @ C a p t u r e D B N a m e ) + ' . s y s . s p _ h e l p p u b l i c a t i o n '  
 - - u s e   t h e   a b o v e   t o   l o o p   t h r o u g h   t h e   l i s t   o f   C A p t u r e D B N a m e s   t o   b r i n g   b a c k   t h e   C G   P u b l i c a t i o n s  
  
  
 S E T   @ S Q L   =   N ' E x e c   ' + Q U O T E N A M E ( @ C a p t u r e D B N a m e ) + ' . s y s . s p _ r e p l f l u s h '  
 	 e x e c   s y s . s p _ E x e c u t e s q l     @ S Q L  
  
 	   - -   s t a r t   r e p l i c a t i o n  
 S E T   @ S Q L   =   N ' e x e c   ' +   Q U O T E N A M E ( @ C a p t u r e D B N a m e )   + ' . s y s . s p _ r e p l i c a t i o n d b o p t i o n    
 	 @ d b n a m e   =   ' +   Q U O T E N A M E ( @ C a p t u r e D B N a m e )   + '  
 	 ,   @ o p t n a m e   =   N ' ' p u b l i s h ' '  
 	 ,   @ v a l u e   =   N ' ' t r u e ' ' '  
  
 E X E C   s y s . s p _ e x e c u t e s q l   @ S Q L  
  
  
 S E T   @ S Q L   =   N ' e x e c   ' +   Q U O T E N A M E ( @ C a p t u r e D B N a m e )   + ' . s y s . s p _ a d d l o g r e a d e r _ a g e n t    
 	 @ j o b _ l o g i n   =   N ' ' E R T C L I N I C A L \ g t h _ s q l r e p l ' '  
 	 ,   @ j o b _ p a s s w o r d   =   ' ' | * N h & E + 9 f 2 T S 8 : 9 s ' '  
 	 ,   @ p u b l i s h e r _ s e c u r i t y _ m o d e   =   1  
 	 ,   @ j o b _ n a m e   =   n u l l '  
  
 	 - - P R I N T   @ S Q L  
 E X E C   s y s . s p _ e x e c u t e s q l   @ S Q L  
  
  
 - -   A d d i n g   t h e   t r a n s a c t i o n a l   p u b l i c a t i o n  
 S E T   @ S Q L   =   N ' e x e c   ' +   Q U O T E N A M E ( @ C a p t u r e D B N a m e )   + ' . s y s . s p _ a d d p u b l i c a t i o n    
 	 @ p u b l i c a t i o n   =   ' +   Q U O T E N A M E ( @ P u b l i c a t i o n N a m e )   + '  
 	 ,   @ d e s c r i p t i o n   =   N ' ' T r a n s a c t i o n a l   p u b l i c a t i o n   o f   d a t a b a s e   ' +   @ C a p t u r e D B N a m e   + '   f r o m   P u b l i s h e r   D B - C H 3 G T R 0 1 \ P R D S Q C 0 1 . ' '  
 	 ,   @ s y n c _ m e t h o d   =   N ' ' c o n c u r r e n t ' '  
 	 ,   @ r e t e n t i o n   =   0  
 	 ,   @ a l l o w _ p u s h   =   N ' ' t r u e ' '  
 	 ,   @ a l l o w _ p u l l   =   N ' ' t r u e ' '  
 	 ,   @ a l l o w _ a n o n y m o u s   =   N ' ' t r u e ' '  
 	 ,   @ e n a b l e d _ f o r _ i n t e r n e t   =   N ' ' f a l s e ' '  
 	 ,   @ s n a p s h o t _ i n _ d e f a u l t f o l d e r   =   N ' ' t r u e ' '  
 	 ,   @ c o m p r e s s _ s n a p s h o t   =   N ' ' f a l s e ' '  
 	 ,   @ f t p _ p o r t   =   2 1  
 	 ,   @ a l l o w _ s u b s c r i p t i o n _ c o p y   =   N ' ' f a l s e ' '  
 	 ,   @ a d d _ t o _ a c t i v e _ d i r e c t o r y   =   N ' ' f a l s e ' '  
 	 ,   @ r e p l _ f r e q   =   N ' ' c o n t i n u o u s ' '  
 	 ,   @ s t a t u s   =   N ' ' a c t i v e ' '  
 	 ,   @ i n d e p e n d e n t _ a g e n t   =   N ' ' t r u e ' '  
 	 ,   @ i m m e d i a t e _ s y n c   =   N ' ' t r u e ' '  
 	 ,   @ a l l o w _ s y n c _ t r a n   =   N ' ' f a l s e ' '  
 	 ,   @ a l l o w _ q u e u e d _ t r a n   =   N ' ' f a l s e ' '  
 	 ,   @ a l l o w _ d t s   =   N ' ' f a l s e ' '  
 	 ,   @ r e p l i c a t e _ d d l   =   1  
 	 ,   @ a l l o w _ i n i t i a l i z e _ f r o m _ b a c k u p   =   N ' ' f a l s e ' '  
 	 ,   @ e n a b l e d _ f o r _ p 2 p   =   N ' ' f a l s e ' '  
 	 ,   @ e n a b l e d _ f o r _ h e t _ s u b   =   N ' ' f a l s e ' ' '  
  
 	 - - P R I N T   @ S Q L  
 	 E X E C   s y s . s p _ e x e c u t e s q l   @ S Q L  
  
 S E T   @ S Q L   =   N ' e x e c   ' +   Q U O T E N A M E ( @ C a p t u r e D B N a m e )   + ' . s y s . s p _ a d d p u b l i c a t i o n _ s n a p s h o t    
 	 @ p u b l i c a t i o n   = ' +   Q U O T E N A M E ( @ P u b l i c a t i o n N a m e )   + '  
 	 ,   @ f r e q u e n c y _ t y p e   =   1  
 	 ,   @ f r e q u e n c y _ i n t e r v a l   =   1  
 	 ,   @ f r e q u e n c y _ r e l a t i v e _ i n t e r v a l   =   1  
 	 ,   @ f r e q u e n c y _ r e c u r r e n c e _ f a c t o r   =   0  
 	 ,   @ f r e q u e n c y _ s u b d a y   =   8  
 	 ,   @ f r e q u e n c y _ s u b d a y _ i n t e r v a l   =   1  
 	 ,   @ a c t i v e _ s t a r t _ t i m e _ o f _ d a y   =   0  
 	 ,   @ a c t i v e _ e n d _ t i m e _ o f _ d a y   =   2 3 5 9 5 9  
 	 ,   @ a c t i v e _ s t a r t _ d a t e   =   0  
 	 ,   @ a c t i v e _ e n d _ d a t e   =   0  
 	 ,   @ j o b _ l o g i n   =   N ' ' E R T C L I N I C A L \ g t h _ s q l r e p l ' '  
 	 ,   @ j o b _ p a s s w o r d   =   ' ' | * N h & E + 9 f 2 T S 8 : 9 s ' '  
 	 ,   @ p u b l i s h e r _ s e c u r i t y _ m o d e   =   1 '  
  
 	 E X E C   s y s . s p _ e x e c u t e s q l   @ S Q L  
  
 S E T   @ S Q L   =   N ' e x e c   ' +   Q U O T E N A M E ( @ C a p t u r e D B N a m e )   + ' . s y s . s p _ a d d a r t i c l e    
 	 @ p u b l i c a t i o n   =   ' +   Q U O T E N A M E ( @ P u b l i c a t i o n N a m e )   + '  
 	 ,   @ a r t i c l e   =   N ' ' A n s w e r s ' '  
 	 ,   @ s o u r c e _ o w n e r   =   N ' ' d b o ' '  
 	 ,   @ s o u r c e _ o b j e c t   =   N ' ' A n s w e r s ' '  
 	 ,   @ t y p e   =   N ' ' l o g b a s e d ' '  
 	 ,   @ d e s c r i p t i o n   =   n u l l  
 	 ,   @ c r e a t i o n _ s c r i p t   =   n u l l  
 	 ,   @ p r e _ c r e a t i o n _ c m d   =   N ' ' d r o p ' '  
 	 ,   @ s c h e m a _ o p t i o n   =   0 x 0 0 0 0 0 0 0 0 0 8 0 3 5 0 9 F  
 	 ,   @ i d e n t i t y r a n g e m a n a g e m e n t o p t i o n   =   N ' ' m a n u a l ' '  
 	 ,   @ d e s t i n a t i o n _ t a b l e   =   N ' ' A n s w e r s ' '  
 	 ,   @ d e s t i n a t i o n _ o w n e r   =   N ' ' d b o ' '  
 	 ,   @ v e r t i c a l _ p a r t i t i o n   =   N ' ' f a l s e ' '  
 	 ,   @ i n s _ c m d   =   N ' ' C A L L   s p _ M S i n s _ d b o A n s w e r s ' '  
 	 ,   @ d e l _ c m d   =   N ' ' C A L L   s p _ M S d e l _ d b o A n s w e r s ' '  
 	 ,   @ u p d _ c m d   =   N ' ' S C A L L   s p _ M S u p d _ d b o A n s w e r s ' ' '  
  
 	 - - P R I N T   @ S Q L  
 	 E X E C   s y s . s p _ e x e c u t e s q l   @ S Q L  
  
 S E T   @ S Q L   =   N ' e x e c   ' +   Q U O T E N A M E ( @ C a p t u r e D B N a m e )   + ' . s y s . s p _ a d d a r t i c l e    
 	 @ p u b l i c a t i o n   =   ' +   Q U O T E N A M E ( @ P u b l i c a t i o n N a m e )   + '  
 	 ,   @ a r t i c l e   =   N ' ' C o m p l e t e d Q u e s t i o n n a i r e s ' '  
 	 ,   @ s o u r c e _ o w n e r   =   N ' ' d b o ' '  
 	 ,   @ s o u r c e _ o b j e c t   =   N ' ' C o m p l e t e d Q u e s t i o n n a i r e s ' '  
 	 ,   @ t y p e   =   N ' ' l o g b a s e d ' '  
 	 ,   @ d e s c r i p t i o n   =   n u l l  
 	 ,   @ c r e a t i o n _ s c r i p t   =   n u l l  
 	 ,   @ p r e _ c r e a t i o n _ c m d   =   N ' ' d r o p ' '  
 	 ,   @ s c h e m a _ o p t i o n   =   0 x 0 0 0 0 0 0 0 0 0 8 0 3 5 0 9 F  
 	 ,   @ i d e n t i t y r a n g e m a n a g e m e n t o p t i o n   =   N ' ' m a n u a l ' '  
 	 ,   @ d e s t i n a t i o n _ t a b l e   =   N ' ' C o m p l e t e d Q u e s t i o n n a i r e s ' '  
 	 ,   @ d e s t i n a t i o n _ o w n e r   =   N ' ' d b o ' '  
 	 ,   @ v e r t i c a l _ p a r t i t i o n   =   N ' ' f a l s e ' '  
 	 ,   @ i n s _ c m d   =   N ' ' C A L L   s p _ M S i n s _ d b o C o m p l e t e d Q u e s t i o n n a i r e s ' '  
 	 ,   @ d e l _ c m d   =   N ' ' C A L L   s p _ M S d e l _ d b o C o m p l e t e d Q u e s t i o n n a i r e s ' '  
 	 ,   @ u p d _ c m d   =   N ' ' S C A L L   s p _ M S u p d _ d b o C o m p l e t e d Q u e s t i o n n a i r e s ' ' '  
  
 	 - - P R I N T   @ S Q L  
 	 E X E C   s y s . s p _ e x e c u t e s q l   @ S Q L  
  
 S E T   @ S Q L   =   N ' e x e c   ' +   Q U O T E N A M E ( @ C a p t u r e D B N a m e )   + ' . s y s . s p _ a d d a r t i c l e    
 	 @ p u b l i c a t i o n   =   ' +   Q U O T E N A M E ( @ P u b l i c a t i o n N a m e )   + '  
 	 ,   @ a r t i c l e   =   N ' ' L o g A n s w e r s ' '  
 	 ,   @ s o u r c e _ o w n e r   =   N ' ' d b o ' '  
 	 ,   @ s o u r c e _ o b j e c t   =   N ' ' L o g A n s w e r s ' '  
 	 ,   @ t y p e   =   N ' ' l o g b a s e d ' '  
 	 ,   @ d e s c r i p t i o n   =   n u l l  
 	 ,   @ c r e a t i o n _ s c r i p t   =   n u l l  
 	 ,   @ p r e _ c r e a t i o n _ c m d   =   N ' ' d r o p ' '  
 	 ,   @ s c h e m a _ o p t i o n   =   0 x 0 0 0 0 0 0 0 0 0 8 0 3 5 0 9 F  
 	 ,   @ i d e n t i t y r a n g e m a n a g e m e n t o p t i o n   =   N ' ' m a n u a l ' '  
 	 ,   @ d e s t i n a t i o n _ t a b l e   =   N ' ' L o g A n s w e r s ' '  
 	 ,   @ d e s t i n a t i o n _ o w n e r   =   N ' ' d b o ' '  
 	 ,   @ v e r t i c a l _ p a r t i t i o n   =   N ' ' f a l s e ' '  
 	 ,   @ i n s _ c m d   =   N ' ' C A L L   s p _ M S i n s _ d b o L o g A n s w e r s ' '  
 	 ,   @ d e l _ c m d   =   N ' ' C A L L   s p _ M S d e l _ d b o L o g A n s w e r s ' '  
 	 ,   @ u p d _ c m d   =   N ' ' S C A L L   s p _ M S u p d _ d b o L o g A n s w e r s ' ' '  
  
 	 E X E C   s y s . s p _ e x e c u t e s q l   @ S Q L  
  
 S E T   @ S Q L   =   N ' e x e c   ' +   Q U O T E N A M E ( @ C a p t u r e D B N a m e )   + ' . s y s . s p _ a d d a r t i c l e    
 	 @ p u b l i c a t i o n   =   ' +   Q U O T E N A M E ( @ P u b l i c a t i o n N a m e )   + '  
 	 ,   @ a r t i c l e   =   N ' ' L o g C o m p l e t e d Q u e s t i o n n a i r e s ' '  
 	 ,   @ s o u r c e _ o w n e r   =   N ' ' d b o ' '  
 	 ,   @ s o u r c e _ o b j e c t   =   N ' ' L o g C o m p l e t e d Q u e s t i o n n a i r e s ' '  
 	 ,   @ t y p e   =   N ' ' l o g b a s e d ' '  
 	 ,   @ d e s c r i p t i o n   =   n u l l  
 	 ,   @ c r e a t i o n _ s c r i p t   =   n u l l  
 	 ,   @ p r e _ c r e a t i o n _ c m d   =   N ' ' d r o p ' '  
 	 ,   @ s c h e m a _ o p t i o n   =   0 x 0 0 0 0 0 0 0 0 0 8 0 3 5 0 9 F  
 	 ,   @ i d e n t i t y r a n g e m a n a g e m e n t o p t i o n   =   N ' ' m a n u a l ' '  
 	 ,   @ d e s t i n a t i o n _ t a b l e   =   N ' ' L o g C o m p l e t e d Q u e s t i o n n a i r e s ' '  
 	 ,   @ d e s t i n a t i o n _ o w n e r   =   N ' ' d b o ' '  
 	 ,   @ v e r t i c a l _ p a r t i t i o n   =   N ' ' f a l s e ' '  
 	 ,   @ i n s _ c m d   =   N ' ' C A L L   s p _ M S i n s _ d b o L o g C o m p l e t e d Q u e s t i o n n a i r e s ' '  
 	 ,   @ d e l _ c m d   =   N ' ' C A L L   s p _ M S d e l _ d b o L o g C o m p l e t e d Q u e s t i o n n a i r e s ' '  
 	 ,   @ u p d _ c m d   =   N ' ' S C A L L   s p _ M S u p d _ d b o L o g C o m p l e t e d Q u e s t i o n n a i r e s ' ' '  
  
 	 E X E C   s y s . s p _ e x e c u t e s q l   @ S Q L  
  
  
 S E T   @ S Q L   =   N ' E X E C   '   + Q U O T E N A M E ( @ C a p t u r e D B N a m e )   +    
 ' . s y s . s p _ a d d s u b s c r i p t i o n    
 	 @ p u b l i c a t i o n   =   ' +   Q U O T E N A M E ( @ P u b l i c a t i o n N a m e )   + '  
 	 ,   @ s u b s c r i b e r   =   ' +   Q U O T E N A M E ( @ R e p o r t S e r v e r )   + '  
 	 ,   @ d e s t i n a t i o n _ d b   =   ' +   Q U O T E N A M E ( @ R e p o r t i n g D B N a m e )   + '  
 	 ,   @ s u b s c r i p t i o n _ t y p e   =   N ' ' P u s h ' '  
 	 ,   @ s y n c _ t y p e   =   N ' ' a u t o m a t i c ' '  
 	 ,   @ a r t i c l e   =   N ' ' a l l ' '  
 	 ,   @ u p d a t e _ m o d e   =   N ' ' r e a d   o n l y ' '  
 	 ,   @ s u b s c r i b e r _ t y p e   =   0 '  
  
 	 E X E C   s y s . s p _ e x e c u t e s q l   @ S Q L  
  
  
 S E T   @ S Q L   =   N ' e x e c   '   + Q U O T E N A M E ( @ C a p t u r e D B N a m e )   + ' . s y s . s p _ a d d p u s h s u b s c r i p t i o n _ a g e n t    
 	 @ p u b l i c a t i o n   =   ' +   Q U O T E N A M E ( @ P u b l i c a t i o n N a m e )   + '  
 	 ,   @ s u b s c r i b e r   =   ' +   Q U O T E N A M E ( @ R e p o r t S e r v e r )   + '  
 	 ,   @ s u b s c r i b e r _ d b   =   ' +   Q U O T E N A M E ( @ R e p o r t i n g D B N a m e )   + '  
 	 ,   @ j o b _ l o g i n   =   N ' ' E R T C L I N I C A L \ g t h _ s q l r e p l ' '  
 	 ,   @ j o b _ p a s s w o r d   =   ' ' | * N h & E + 9 f 2 T S 8 : 9 s ' '  
 	 ,   @ s u b s c r i b e r _ s e c u r i t y _ m o d e   =   1  
 	 ,   @ f r e q u e n c y _ t y p e   =   6 4  
 	 ,   @ f r e q u e n c y _ i n t e r v a l   =   0  
 	 ,   @ f r e q u e n c y _ r e l a t i v e _ i n t e r v a l   =   0  
 	 ,   @ f r e q u e n c y _ r e c u r r e n c e _ f a c t o r   =   0  
 	 ,   @ f r e q u e n c y _ s u b d a y   =   0  
 	 ,   @ f r e q u e n c y _ s u b d a y _ i n t e r v a l   =   0  
 	 ,   @ a c t i v e _ s t a r t _ t i m e _ o f _ d a y   =   0  
 	 ,   @ a c t i v e _ e n d _ t i m e _ o f _ d a y   =   2 3 5 9 5 9  
 	 ,   @ a c t i v e _ s t a r t _ d a t e   =   2 0 1 7 1 1 2 1  
 	 ,   @ a c t i v e _ e n d _ d a t e   =   9 9 9 9 1 2 3 1  
 	 ,   @ e n a b l e d _ f o r _ s y n c m g r   =   N ' ' F a l s e ' '  
 	 ,   @ d t s _ p a c k a g e _ l o c a t i o n   =   N ' ' D i s t r i b u t o r ' '  
 	 '  
 	 E X E C   s y s . s p _ e x e c u t e s q l   @ S Q L  
  
  
 S E T   @ S Q L   =   N ' e x e c   '   + Q U O T E N A M E ( @ C a p t u r e D B N a m e )   + ' . s y s . 	    
 	   s p _ s t a r t p u b l i c a t i o n _ s n a p s h o t   @ p u b l i c a t i o n   =   ' +   Q U O T E N A M E ( @ P u b l i c a t i o n N a m e )  
 	    
 	   E X E C   s y s . s p _ e x e c u t e s q l   @ S Q L  
  
 	 E N D  
 E L S E  
   P R I N T   ' T h e   C a p t u r e   D a t a b a s e :   '   + @ C a p t u r e D B N a m e   +   '   d o e s   n o t   e x i s t ,   t h u s   t h e   R e p l i c a t i o n   C A N N O T   b e   r e - i n s t a t e d . '  
  
  
 F E T C H   N E X T   F R O M   C G D B c s r   I N T O   @ E x c o I d e n t i f i e r , @ S t u d y E n v i r o n m e n t ,   @ C a p t u r e D B N a m e  
 E N D  
  
 C L O S E   C G D B c s r  
 D E A L L O C A T E   C G D B c s r  
  
 D R O P   t a b l e   # L i v e _ U a t _ S t u d i e s  
    
  
  
 C O M M I T   T R A N  
 P R I N T   ' S u c c e s s f u l l y   e x e c u t e d   s t o r e d   p r o c e d u r e :   C a p t u r e G a t e w a y R e p l i c a t i o n F o r D R '  
  
 E N D   T R Y  
 B E G I N   C A T C H  
 	 R O L L B A C K   T R A N  
 	 P R I N T   ' F A I L E D   t o   E X E C U T E   s t o r e d   p r o c :   C a p t u r e G a t e w a y R e p l i c a t i o n F o r D R   a n d   T R A N S A C T I O N   R O L L E D   B A C K '  
 	 P R I N T   E R R O R _ M E S S A G E ( )  
 E N D   C A T C H 