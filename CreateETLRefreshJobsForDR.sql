
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ========================================================================================
-- Author:		Savina Sarath
-- Create date: 11 Feb 2019
-- Description:	Re-instating the ETL-Refresh jobs for all the Reporting databases 
--				in the event of a DR scenario.
-- Revision History: 
-- =========================================================================================

CREATE PROCEDURE [dbo].[CreateETLRefreshJobsForDR]
AS


BEGIN TRAN

DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
DECLARE @SQL NVARCHAR(MAX)
DECLARE @StudyState INT
DECLARE @StudyEnvironment VARCHAR(20) 
DECLARE @ExcoIdentifier NVARCHAR(MAX)
DECLARE @DBStateName NVARCHAR(100)
DECLARE @ReportingDBName NVARCHAR(MAX)
DECLARE @ReportServer NVARCHAR(MAX) = 'C1DGTRVDB02'
DECLARE @DBCoreName NVARCHAR(100) = 'Gather_R3'
DECLARE @JobName AS NVARCHAR(50) = N'STUDY-' + @ReportingDBName + '-Refresh'
DECLARE @SysCategory AS NVARCHAR(50) = N'[ETL Maintenance]'
DECLARE @ScheduleUID AS NVARCHAR(50) = CAST(NEWID() AS NVARCHAR(50))

--Selecting all the studies in Live states into #Live_Uat_Studies
select gt.ExcoIdentifier
,si.Active
,si.StudyEnvironment As StudyState
 INTO #Live_Uat_Studies 
 from Gather_R3_Live..StudyInstances si 
JOIN Gather_R3..GatherStudies  gt ON si.GatherStudyId = gt.GatherStudyId 
WHERE si.Active = 1

--Selecting all the studies in UAT states into #Live_Uat_Studies
INSERT INTO 
#Live_Uat_Studies
(ExcoIdentifier
,Active
,StudyState

)
select gt.ExcoIdentifier
,si.Active
,si.StudyEnvironment 
from Gather_R3_Uat..StudyInstances si 
JOIN Gather_R3..GatherStudies  gt ON si.GatherStudyId = gt.GatherStudyId 
WHERE si.Active = 1

--Declaring cursor to loop through the temp table to re-instate ETL-Refresh jobs for all the all the Reporting Databases
DECLARE CGDBcsr CURSOR FAST_FORWARD FOR
SELECT ExcoIdentifier ,StudyState FROM #Live_Uat_Studies 
OPEN CGDBcsr
FETCH NEXT FROM CGDBcsr INTO @ExcoIdentifier,@StudyState

WHILE @@FETCH_STATUS = 0
BEGIN
SET @StudyEnvironment = CASE @StudyState
				WHEN 0
					THEN 'Design'
				WHEN 1
					THEN 'Uat'
				WHEN 2
					THEN 'Live'
				END

SET @ReportingDBName = CONCAT (
							@ExcoIdentifier
							,'_'
							,@StudyEnvironment
							,'_Reporting'
							)

--Checking if the Reporting Database has been re-instated 
IF EXISTS (SELECT name 
  FROM sys.databases WHERE name = @ReportingDBName)
BEGIN 

SET @DBStateName = CASE @StudyEnvironment
				WHEN 'Design'
				THEN 'Gather_R3_Design'
				WHEN 'Uat'
				THEN 'Gather_R3_Uat'
				WHEN 'Live'
				THEN 'Gather_R3_Live'
				END

-- Delete job if it already exists:
IF EXISTS(SELECT job_id FROM msdb.dbo.sysjobs WHERE (name = @JobName))
BEGIN
    EXEC msdb.dbo.sp_delete_job
        @job_name = @JobName;
END

IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=@SysCategory AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=@SysCategory
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=@JobName, 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=2, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=@SysCategory, 
		@owner_login_name=N'sa',
		@notify_email_operator_name=N'Engagement SQL DBAs', 
		@job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback


--Start of the POPULATE job, to refresh data in the ETL tables
SET @SQL = N'
SET QUOTED_IDENTIFIER ON;
			EXECUTE [dbo].[prcETL_ExecuteAllTheSPs] 
			   ''' + @ExcoIdentifier + '''
			  ,' + CAST(@StudyState AS NVARCHAR(1)) + '
			  ,''' + @DBCoreName + '''
			  ,''' + @DBStateName + '''
			  ,''' + @ReportingDBName + '''
			  ,'''+ @ReportServer +'''
			GO'
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Populate Data', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command= @SQL , 
		@database_name=N'ETLResource', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1


--**Start of falttened view creation job. **
SET @SQL = CONVERT(nvarchar(max),N'') + N'SET QUOTED_IDENTIFIER ON
	DECLARE @QuestionnaireGlobalIdentifier NVARCHAR(MAX)
	DECLARE @QuestionGlobalId NVARCHAR(MAX) 
	DECLARE @ViewName NVARCHAR(MAX)
	DECLARE @View NVARCHAR(MAX)
	DECLARE @TotalRows iNT
	DECLARE @Current INT = 0
	DECLARE @QuestionName NVARCHAR(MAX)
	
	DECLARE Questionnaire CURSOR FAST_FORWARD
	FOR
	SELECT distinct QuestionnaireGlobalIdentifier
		FROM ['+@ReportServer+ '].[' + @ReportingDBName + '].dbo.tbl_ABPQuestionnaires 
 OPEN Questionnaire
	FETCH NEXT
		FROM Questionnaire
	 INTO @QuestionnaireGlobalIdentifier
--Cursor through the tbl_ABPQuestionnaires for creating view name per Questionnaire
	WHILE @@FETCH_STATUS = 0
	BEGIN
	SET @View =
	( SELECT  [ETLResource].dbo.[RemoveSpecialChars](rtrim(ltrim(name))) + case when RowNum = 1 then '''' else ''_'' + CONVERT(varchar(100), RowNum-1) end 
	from (
	select ABPQuestionnaireName as ''Name'',
		   ROW_NUMBER() over (partition by ABPQuestionnaireName order by QuestionnaireGlobalIdentifier) as "RowNum",
			QuestionnaireGlobalIdentifier
	FROM ['+@ReportServer+ '].[' + @ReportingDBName + '].dbo.tbl_ABPQuestionnaires 
	where QuestionnaireGlobalIdentifier = @QuestionnaireGlobalIdentifier
	) x)	
		SET @ViewName = CONCAT (
					''Vw_''
				,@View
				,''_Answers''
				)

DECLARE @TabQuestionGlobalID TABLE
	(
	RowNumber INT IDENTITY(1,1)
	,QuestionName NVARCHAR(MAX)
	,QuestionGID NVARCHAR(MAX)
	)
  INSERT INTO @TabQuestionGlobalID 
  select  [ETLResource].dbo.[RemoveSpecialChars](rtrim(ltrim(name))) + case when RowNum = 1 then '''' else ''_'' + CONVERT(varchar(100), RowNum-1) end as QuestionName
	,QuestionGlobalIdentifier as QuestionGID
	from (
	select ABPQuestionName as ''Name'',
		   ROW_NUMBER() over (partition by ABPQuestionName order by QuestionGlobalIdentifier) as "RowNum",
			QuestionGlobalIdentifier
	FROM ['+@ReportServer+ '].[' + @ReportingDBName + '].dbo.tbl_ABQuestions 
		WHERE QuestionnaireGlobalIdentifier =  @QuestionnaireGlobalIdentifier
	) x
--Creating the View by dropping them if already exists
		select COLUMN_NAME
		INTO #ViewColumns
		from INFORMATION_SCHEMA .COLUMNS  where TABLE_NAME =  @ViewName 
		AND TABLE_SCHEMA = ''dbo''
		SET @TotalRows = 0
			SET @Current = 0
				SET @TotalRows = @@ROWCOUNT
		DECLARE @QGIDCount  INT 
				SET @QGIDCount = (select count(*) from @TabQuestionGlobalID)  + 9 --9 is the total number of standard cols in the Views
				DECLARE @VwColCount  INT 
				SET @VwColCount = (select count(COLUMN_NAME) FROM #ViewColumns )	
		--IF the QuestionGID is do not exist in the Column_name list of the existing view then drop the view and recreate	
			IF (select count(QuestionGID) from @TabQuestionGlobalID 
			WHERE QuestionName NOT IN (select COLUMN_NAME FROM #ViewColumns ) 
			) > 0 OR (@QGIDCount != @VwColCount)
			BEGIN
			--DROP VIEW, if the Columns in the existing view doesnt match with the list of QuestionGlobalIds in the new published questionnaire versions
			EXEC ['+@ReportServer+ '].[ReportResource].dbo.[prcETL_DropView]
			@ReportServer= ['+@ReportServer +'], 
			@DBName = '+@ReportingDBName +' ,
			@ViewName = @ViewName
		WHILE @Current < @TotalRows
		BEGIN
		SET @QuestionGlobalId = STUFF((SELECT distinct '','' + QUOTENAME(QuestionGID)
           FROM @TabQuestionGlobalID
           FOR XML PATH(''''), TYPE
           ).value(''.'', ''NVARCHAR(MAX)'') 
			,1,1,'''')

		SET @QuestionName = STUFF((SELECT distinct '','' + QUOTENAME(QuestionGID) + '' as ''+ QUOTENAME(QuestionName)
		 FROM @TabQuestionGlobalID
			FOR XML PATH(''''), TYPE
		   ).value(''.'', ''NVARCHAR(MAX)'') 
			,1,1,'''')
		SET @Current = @Current + 1	
		END	
		END
		--Create the  view
		EXEC ['+@ReportServer+ '].[ReportResource].dbo.[prcETL_Vw_FlattenedQuestionnaire_Create_Names] 
		@ReportServer = ['+@ReportServer+ '], 
		@DBName = '+@ReportingDBName +' ,
		@ViewName = @ViewName, 
		@QuestionnaireGlobalIdentifier = @QuestionnaireGlobalIdentifier, 
		@QuestionGlobalId =@QuestionGlobalId,
		@QuestionName = @QuestionName
	DELETE FROM @TabQuestionGlobalID
	DROP TABLE #ViewColumns
FETCH NEXT
		FROM Questionnaire
	 INTO @QuestionnaireGlobalIdentifier
END
CLOSE Questionnaire
DEALLOCATE Questionnaire'
	
--PRINT @SQL
--***End of View Creation*****	
	
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Create Named Views', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem= N'TSQL', 
		@command=@SQL, 
		--@database_name=N'ETLResource', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback


--**End of view creation- Job step 3**


IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Minute by minute', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=4, 
		@freq_subday_interval=1, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20170921, 
		@active_end_date=99991231, 
		@active_start_time=0, 
		@active_end_time=235959, 
		@schedule_uid=@ScheduleUID
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END
ELSE
 PRINT 'The Reproting Database: ' +@ReportingDBName + ' does not exist, thus the ETL-Refresh Job CANNOT be re-instated.'
FETCH NEXT FROM CGDBcsr INTO @ExcoIdentifier, @StudyState

END

CLOSE CGDBcsr
DEALLOCATE CGDBcsr
DROP table #Live_Uat_Studies

COMMIT TRANSACTION
PRINT 'Successfully executed stored procedure: CreateRefreshJobsForDR'

GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
	PRINT 'FAILED to EXECUTE stored proc: CreateRefreshJobsForDR and TRANSACTION ROLLED BACK'
	PRINT ERROR_MESSAGE()
EndSave:








 