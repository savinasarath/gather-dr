USE [Gather_R3_Live]
GO
/****** Object:  StoredProcedure [dbo].[AuditDBSchemaUpdates]    Script Date: 2/11/2019 1:51:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =====================================================
-- Author:		Savina Dasan
-- Create date: 21 Jan 2019
-- Description:	This script re-instates auditing for schema changes in the source database
-- Revision History: 
--==================Test strings========================
-- EXEC AuditDBSchemaUpdates  --This will re-instate Gather_R3_Live_Audit database with the schema changes to match Gather_R3_Live
-- =====================================================



CREATE PROCEDURE [dbo].[AuditDBSchemaUpdates]
AS
BEGIN

DECLARE @DBName NVARCHAR(200)= 'Gather_R3_Live'
DECLARE @AuditDBName NVARCHAR(MAX) = CONCAT(@DBName,'_Audit')

DECLARE @tableName NVARCHAR(MAX),
		@columnName NVARCHAR(MAX),
		@triggerPrefix NVARCHAR(MAX),
		@columns NVARCHAR(MAX),
		@sql NVARCHAR(MAX),
		@dataType NVARCHAR(128)

SELECT	@triggerPrefix = 'UPDATE_INSERT_DELETE_trig_',
		@columns = ' '

-- create temp audit table
CREATE TABLE #audit
(
	audit_id UNIQUEIDENTIFIER NOT NULL,
	audit_user NVARCHAR(32),
	audit_time DateTime,
	audit_action NVARCHAR(8)
)

-- Get all tables in Gather
DECLARE table_cursor CURSOR FOR
( 
	SELECT TABLE_NAME FROM [information_schema].[TABLES] 
	WHERE TABLE_NAME NOT LIKE '%MIGRATION%'
	  AND TABLE_NAME NOT LIKE 'MS%'
	  AND TABLE_NAME NOT LIKE 'sync%'
	  AND TABLE_NAME NOT LIKE 'sys%' 
	   AND TABLE_TYPE = 'BASE TABLE'
)
OPEN table_cursor

FETCH NEXT FROM table_cursor INTO @tableName
WHILE @@FETCH_STATUS = 0
BEGIN
-- ----Check if any tables are missing from Gather Audit
	SELECT @sql =
	N'IF NOT EXISTS( SELECT * FROM ['+@AuditDBName+'].[INFORMATION_SCHEMA].[TABLES]  WHERE TABLE_NAME = '''+@tableName +''') '+ 
	N'BEGIN ' +
		 N' SELECT t1.*, t.* '+
		 N' INTO ['+@AuditDBName+']..[' + @tableName + N']' +
		 N' FROM  '+ @tableName +' AS t '+
		N' JOIN #audit AS t1 '+ -- add column names from temp audit table 
		N' ON 1 = 2 '+
		N' END	'
	
	EXEC sp_executesql @sql
	
	-- collect column names
	DECLARE column_cursor CURSOR FOR
	(
		SELECT COLUMN_NAME, DATA_TYPE 
		FROM information_schema.columns						 
		WHERE TABLE_NAME = @tableName		
	)
	OPEN column_cursor

	SET @columns = ' '	
	FETCH NEXT FROM column_cursor INTO @columnName, @dataType
	WHILE @@FETCH_STATUS = 0
	BEGIN
	
	-- look for missing columns in the audit tables
		SET @sql =
		N'IF NOT EXISTS (SELECT * FROM ['+@AuditDBName+'].[INFORMATION_SCHEMA].[COLUMNS] WHERE COLUMN_NAME = '''+ @columnName +''' AND TABLE_NAME = '''+@tableName +'''	) ' +
		N'BEGIN ' +
			-- add missing columns
		N' ALTER TABLE ['+@AuditDBName+']..[' + @tableName + N']' +
		N' ADD [' + @columnName + N']  ' + @dataType  +
		N' END'

		EXEC sp_executesql @sql
				
		SET @Columns = @Columns + ', ' + '[' + @ColumnName + ']'

		FETCH NEXT FROM column_cursor
		INTO @ColumnName, @dataType		
	END
	CLOSE column_cursor
	DEALLOCATE column_cursor
		
	-- Drop triggers from Gather
	SELECT @sql =
		N' IF EXISTS (SELECT * FROM SYSOBJECTS WHERE NAME = ''' + @triggerPrefix + @tableName + ''' AND TYPE = ''TR'')' +
		N' BEGIN' +
		N' DROP TRIGGER ' + @triggerPrefix + @tableName + 
		N' END'

	EXEC sp_executesql @sql

	-- create trigger
	SELECT @sql = 
	N' CREATE TRIGGER ' + @triggerPrefix + @tableName + 
	N' ON ' + @tableName +
	N' FOR INSERT, UPDATE, DELETE ' + 
	N' AS '	+ 
	N' SET NOCOUNT ON; '+
	N' DECLARE @Type CHAR(6), @UserName VARCHAR(32), @UpdateTime datetime, @ColumnNames NVARCHAR(MAX) ' + 
	N' SELECT @UserName = SYSTEM_USER, @UpdateTime = GetDate() ' + 
	N' IF EXISTS( SELECT * FROM inserted) ' +
		N' IF EXISTS( SELECT * FROM deleted ) ' +
			N' SET @Type = ''UPDATE'' ' +
		N' ELSE ' +
			N' SET @Type = ''INSERT'' ' +
	N' ELSE ' +
			N' SET @Type = ''DELETE'' ' +
	N' IF( @Type = ''UPDATE'' OR @Type = ''INSERT'' ) ' +
	N' BEGIN INSERT INTO ['+@AuditDBName+']..['  + @tableName + ']' +
	N'( audit_id, audit_user, audit_action, audit_time ' + @Columns + ')'
	+' SELECT NEWID(), @UserName, @Type, @UpdateTime ' + @Columns + ' '
	+' FROM inserted as i END '				
	+'	ELSE BEGIN INSERT INTO ['+@AuditDBName+']..[' + @tableName + ']' +
	'( audit_id, audit_user, audit_action, audit_time ' + @Columns + ')'
	+' select NEWID(), @UserName, @Type, @UpdateTime ' + @Columns + ' '
	+' FROM deleted as d END '
	
				
	EXEC sp_executesql @sql		
	
	--Enable triggers for table
	SELECT @sql =  N' ENABLE TRIGGER  [' + @triggerPrefix + @tableName +'] ON [' +@tableName +'];' 
	EXEC sp_executesql @sql	
		
	FETCH NEXT FROM table_cursor INTO @tableName
END
DEALLOCATE table_cursor
DROP TABLE #audit
END



