U S E   [ G a t h e r _ R 3 _ D e s i g n ]  
 G O  
 / * * * * * *   O b j e c t :     S t o r e d P r o c e d u r e   [ d b o ] . [ A u d i t D B S c h e m a U p d a t e s ]         S c r i p t   D a t e :   2 / 1 1 / 2 0 1 9   1 : 4 9 : 2 5   P M   * * * * * * /  
 S E T   A N S I _ N U L L S   O N  
 G O  
 S E T   Q U O T E D _ I D E N T I F I E R   O N  
 G O  
  
  
 - -   = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =  
 - -   A u t h o r : 	 	 S a v i n a   D a s a n  
 - -   C r e a t e   d a t e :   2 1   J a n   2 0 1 9  
 - -   D e s c r i p t i o n : 	 T h i s   s c r i p t   r e - i n s t a t e s   a u d i t i n g   f o r   s c h e m a   c h a n g e s   i n   t h e   s o u r c e   d a t a b a s e  
 - -   R e v i s i o n   H i s t o r y :    
 - - = = = = = = = = = = = = = = = = = = T e s t   s t r i n g s = = = = = = = = = = = = = = = = = = = = = = = =  
 - -   E X E C   A u d i t D B S c h e m a U p d a t e s     - - T h i s   w i l l   r e - i n s t a t e   G a t h e r _ R 3 _ D e s i g n _ A u d i t   d a t a b a s e   w i t h   t h e   s c h e m a   c h a n g e s   t o   m a t c h   G a t h e r _ R 3 _ D e s i g n  
 - -   = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =  
  
  
  
 C R E A T E   P R O C E D U R E   [ d b o ] . [ A u d i t D B S c h e m a U p d a t e s ]  
 A S  
 B E G I N  
  
 D E C L A R E   @ D B N a m e   N V A R C H A R ( 2 0 0 ) =   ' G a t h e r _ R 3 _ D e s i g n '  
 D E C L A R E   @ A u d i t D B N a m e   N V A R C H A R ( M A X )   =   C O N C A T ( @ D B N a m e , ' _ A u d i t ' )  
  
 D E C L A R E   @ t a b l e N a m e   N V A R C H A R ( M A X ) ,  
 	 	 @ c o l u m n N a m e   N V A R C H A R ( M A X ) ,  
 	 	 @ t r i g g e r P r e f i x   N V A R C H A R ( M A X ) ,  
 	 	 @ c o l u m n s   N V A R C H A R ( M A X ) ,  
 	 	 @ s q l   N V A R C H A R ( M A X ) ,  
 	 	 @ d a t a T y p e   N V A R C H A R ( 1 2 8 )  
  
 S E L E C T 	 @ t r i g g e r P r e f i x   =   ' U P D A T E _ I N S E R T _ D E L E T E _ t r i g _ ' ,  
 	 	 @ c o l u m n s   =   '   '  
  
 - -   c r e a t e   t e m p   a u d i t   t a b l e  
 C R E A T E   T A B L E   # a u d i t  
 (  
 	 a u d i t _ i d   U N I Q U E I D E N T I F I E R   N O T   N U L L ,  
 	 a u d i t _ u s e r   N V A R C H A R ( 3 2 ) ,  
 	 a u d i t _ t i m e   D a t e T i m e ,  
 	 a u d i t _ a c t i o n   N V A R C H A R ( 8 )  
 )  
  
 - -   G e t   a l l   t a b l e s   i n   G a t h e r  
 D E C L A R E   t a b l e _ c u r s o r   C U R S O R   F O R  
 (    
 	 S E L E C T   T A B L E _ N A M E   F R O M   [ i n f o r m a t i o n _ s c h e m a ] . [ T A B L E S ]    
 	 W H E R E   T A B L E _ N A M E   N O T   L I K E   ' % M I G R A T I O N % '  
 	     A N D   T A B L E _ N A M E   N O T   L I K E   ' M S % '  
 	     A N D   T A B L E _ N A M E   N O T   L I K E   ' s y n c % '  
 	     A N D   T A B L E _ N A M E   N O T   L I K E   ' s y s % '    
 	         A N D   T A B L E _ T Y P E   =   ' B A S E   T A B L E '  
 )  
 O P E N   t a b l e _ c u r s o r  
  
 F E T C H   N E X T   F R O M   t a b l e _ c u r s o r   I N T O   @ t a b l e N a m e  
 W H I L E   @ @ F E T C H _ S T A T U S   =   0  
 B E G I N  
 - -   - - - - C h e c k   i f   a n y   t a b l e s   a r e   m i s s i n g   f r o m   G a t h e r   A u d i t  
 	 S E L E C T   @ s q l   =  
 	 N ' I F   N O T   E X I S T S (   S E L E C T   *   F R O M   [ ' + @ A u d i t D B N a m e + ' ] . [ I N F O R M A T I O N _ S C H E M A ] . [ T A B L E S ]     W H E R E   T A B L E _ N A M E   =   ' ' ' + @ t a b l e N a m e   + ' ' ' )   ' +    
 	 N ' B E G I N   '   +  
 	 	   N '   S E L E C T   t 1 . * ,   t . *   ' +  
 	 	   N '   I N T O   [ ' + @ A u d i t D B N a m e + ' ] . . [ '   +   @ t a b l e N a m e   +   N ' ] '   +  
 	 	   N '   F R O M     ' +   @ t a b l e N a m e   + '   A S   t   ' +  
 	 	 N '   J O I N   # a u d i t   A S   t 1   ' +   - -   a d d   c o l u m n   n a m e s   f r o m   t e m p   a u d i t   t a b l e    
 	 	 N '   O N   1   =   2   ' +  
 	 	 N '   E N D 	 '  
 	  
 	 E X E C   s p _ e x e c u t e s q l   @ s q l  
 	  
 	 - -   c o l l e c t   c o l u m n   n a m e s  
 	 D E C L A R E   c o l u m n _ c u r s o r   C U R S O R   F O R  
 	 (  
 	 	 S E L E C T   C O L U M N _ N A M E ,   D A T A _ T Y P E    
 	 	 F R O M   i n f o r m a t i o n _ s c h e m a . c o l u m n s 	 	 	 	 	 	    
 	 	 W H E R E   T A B L E _ N A M E   =   @ t a b l e N a m e 	 	  
 	 )  
 	 O P E N   c o l u m n _ c u r s o r  
  
 	 S E T   @ c o l u m n s   =   '   ' 	  
 	 F E T C H   N E X T   F R O M   c o l u m n _ c u r s o r   I N T O   @ c o l u m n N a m e ,   @ d a t a T y p e  
 	 W H I L E   @ @ F E T C H _ S T A T U S   =   0  
 	 B E G I N  
 	  
 	 - -   l o o k   f o r   m i s s i n g   c o l u m n s   i n   t h e   a u d i t   t a b l e s  
 	 	 S E T   @ s q l   =  
 	 	 N ' I F   N O T   E X I S T S   ( S E L E C T   *   F R O M   [ ' + @ A u d i t D B N a m e + ' ] . [ I N F O R M A T I O N _ S C H E M A ] . [ C O L U M N S ]   W H E R E   C O L U M N _ N A M E   =   ' ' ' +   @ c o l u m n N a m e   + ' ' '   A N D   T A B L E _ N A M E   =   ' ' ' + @ t a b l e N a m e   + ' ' ' 	 )   '   +  
 	 	 N ' B E G I N   '   +  
 	 	 	 - -   a d d   m i s s i n g   c o l u m n s  
 	 	 N '   A L T E R   T A B L E   [ ' + @ A u d i t D B N a m e + ' ] . . [ '   +   @ t a b l e N a m e   +   N ' ] '   +  
 	 	 N '   A D D   [ '   +   @ c o l u m n N a m e   +   N ' ]     '   +   @ d a t a T y p e     +  
 	 	 N '   E N D '  
  
 	 	 E X E C   s p _ e x e c u t e s q l   @ s q l  
 	 	 	 	  
 	 	 S E T   @ C o l u m n s   =   @ C o l u m n s   +   ' ,   '   +   ' [ '   +   @ C o l u m n N a m e   +   ' ] '  
  
 	 	 F E T C H   N E X T   F R O M   c o l u m n _ c u r s o r  
 	 	 I N T O   @ C o l u m n N a m e ,   @ d a t a T y p e 	 	  
 	 E N D  
 	 C L O S E   c o l u m n _ c u r s o r  
 	 D E A L L O C A T E   c o l u m n _ c u r s o r  
 	 	  
 	 - -   D r o p   t r i g g e r s   f r o m   G a t h e r  
 	 S E L E C T   @ s q l   =  
 	 	 N '   I F   E X I S T S   ( S E L E C T   *   F R O M   S Y S O B J E C T S   W H E R E   N A M E   =   ' ' '   +   @ t r i g g e r P r e f i x   +   @ t a b l e N a m e   +   ' ' '   A N D   T Y P E   =   ' ' T R ' ' ) '   +  
 	 	 N '   B E G I N '   +  
 	 	 N '   D R O P   T R I G G E R   '   +   @ t r i g g e r P r e f i x   +   @ t a b l e N a m e   +    
 	 	 N '   E N D '  
  
 	 E X E C   s p _ e x e c u t e s q l   @ s q l  
  
 	 - -   c r e a t e   t r i g g e r  
 	 S E L E C T   @ s q l   =    
 	 N '   C R E A T E   T R I G G E R   '   +   @ t r i g g e r P r e f i x   +   @ t a b l e N a m e   +    
 	 N '   O N   '   +   @ t a b l e N a m e   +  
 	 N '   F O R   I N S E R T ,   U P D A T E ,   D E L E T E   '   +    
 	 N '   A S   ' 	 +    
 	 N '   S E T   N O C O U N T   O N ;   ' +  
 	 N '   D E C L A R E   @ T y p e   C H A R ( 6 ) ,   @ U s e r N a m e   V A R C H A R ( 3 2 ) ,   @ U p d a t e T i m e   d a t e t i m e ,   @ C o l u m n N a m e s   N V A R C H A R ( M A X )   '   +    
 	 N '   S E L E C T   @ U s e r N a m e   =   S Y S T E M _ U S E R ,   @ U p d a t e T i m e   =   G e t D a t e ( )   '   +    
 	 N '   I F   E X I S T S (   S E L E C T   *   F R O M   i n s e r t e d )   '   +  
 	 	 N '   I F   E X I S T S (   S E L E C T   *   F R O M   d e l e t e d   )   '   +  
 	 	 	 N '   S E T   @ T y p e   =   ' ' U P D A T E ' '   '   +  
 	 	 N '   E L S E   '   +  
 	 	 	 N '   S E T   @ T y p e   =   ' ' I N S E R T ' '   '   +  
 	 N '   E L S E   '   +  
 	 	 	 N '   S E T   @ T y p e   =   ' ' D E L E T E ' '   '   +  
 	 N '   I F (   @ T y p e   =   ' ' U P D A T E ' '   O R   @ T y p e   =   ' ' I N S E R T ' '   )   '   +  
 	 N '   B E G I N   I N S E R T   I N T O   [ ' + @ A u d i t D B N a m e + ' ] . . [ '     +   @ t a b l e N a m e   +   ' ] '   +  
 	 N ' (   a u d i t _ i d ,   a u d i t _ u s e r ,   a u d i t _ a c t i o n ,   a u d i t _ t i m e   '   +   @ C o l u m n s   +   ' ) '  
 	 + '   S E L E C T   N E W I D ( ) ,   @ U s e r N a m e ,   @ T y p e ,   @ U p d a t e T i m e   '   +   @ C o l u m n s   +   '   '  
 	 + '   F R O M   i n s e r t e d   a s   i   E N D   ' 	 	 	 	  
 	 + ' 	 E L S E   B E G I N   I N S E R T   I N T O   [ ' + @ A u d i t D B N a m e + ' ] . . [ '   +   @ t a b l e N a m e   +   ' ] '   +  
 	 ' (   a u d i t _ i d ,   a u d i t _ u s e r ,   a u d i t _ a c t i o n ,   a u d i t _ t i m e   '   +   @ C o l u m n s   +   ' ) '  
 	 + '   s e l e c t   N E W I D ( ) ,   @ U s e r N a m e ,   @ T y p e ,   @ U p d a t e T i m e   '   +   @ C o l u m n s   +   '   '  
 	 + '   F R O M   d e l e t e d   a s   d   E N D   '  
 	  
 	 	 	 	  
 	 E X E C   s p _ e x e c u t e s q l   @ s q l 	 	  
 	  
 	 - - E n a b l e   t r i g g e r s   f o r   t a b l e  
 	 S E L E C T   @ s q l   =     N '   E N A B L E   T R I G G E R     [ '   +   @ t r i g g e r P r e f i x   +   @ t a b l e N a m e   + ' ]   O N   [ '   + @ t a b l e N a m e   + ' ] ; '    
 	 E X E C   s p _ e x e c u t e s q l   @ s q l 	  
 	 	  
 	 F E T C H   N E X T   F R O M   t a b l e _ c u r s o r   I N T O   @ t a b l e N a m e  
 E N D  
 D E A L L O C A T E   t a b l e _ c u r s o r  
 D R O P   T A B L E   # a u d i t  
 E N D  
  
  
  
 